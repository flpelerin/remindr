## Remindr

    Logiciel de création de rappels et gestion de groupe,
    codé en nodejs et prisma.




### Documentation:

    Toute la documentation se trouve dans le répertoire docs/
    du repository courrant.
    Pour toute information, veuillez vous référer à la documentation.




### Lancement rapide:

    Pour installer toutes les dépendences sur un Debian, utilisez la commande: 

    sudo apt install nodejs npm && npm install express express-handlebars express-session prisma @prisma/client body-parser


    Afin de lancer le serveur sur votre machine, utilisez la commande suivante:

    npx prisma generate && ./build-app.sh && node app.js

