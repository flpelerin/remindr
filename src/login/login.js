app.get ('/login', async (req, res) => {
	res.render ('login/login');
})

app.get ('/login-error', async (req, res) => {
	res.render ('login/login-error');
})



app.post ('/login-reply', async (req, res) =>{
	/* Getting the user data from the POST method of the "/login" route. */
	const name = 		rmquote (JSON.stringify (req.body.username));
	const password = 	rmquote (JSON.stringify (req.body.password));


	/* We try finding a user record given our user data */
	const user = await prisma.user.findFirst({
	  where: {
	    name: name,
	    password: password
	  },
	});

	
	/* If the user doesn't already exist, we redirect to the "login-error" route.
	 * The to-be-user then needs to navigate towards register, which is standard procedure for such web-application. */
	if (!user) {
		res.redirect ("/login-error"); 
		return;
	}


	/* We save this new user onto the session, using a cookie.
	 * This cookie will expire after a certain time, defined in the cookie itself (application middleware).
	 * This forces the user to log back in after some time.
	 * */
	req.session.user = user;
	res.redirect ("/dashboard");
});
