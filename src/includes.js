const express = require ('express')
const app = express ()
const handlebars = require ('express-handlebars');
const engine = handlebars.engine;
const { PrismaClient } = require ('@prisma/client');
const prisma = new PrismaClient ();
const bodyParser = require ('body-parser');
const session = require('express-session');
const port = 3000

app.engine ('handlebars', engine ());
app.set ('view engine', 'handlebars');
app.set ('views', './views');
app.use(bodyParser.urlencoded());
app.use(bodyParser.json());

function rmquote (string) { return string != null ? string.replace ('\"', '').replace ('\"', '') : ""; }
