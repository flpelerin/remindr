app.get ('/register', async (req, res) => {
	res.render ('register/register');
});

app.get ('/register-error', async (req, res) => {
	res.render ('register/register-error');
});


app.post ('/register-reply', async (req, res) =>{

	/* Reading user data from POST method, given by the "/register" route. */
	const name = 			rmquote (JSON.stringify (req.body.username));
	const email = 			rmquote (JSON.stringify (req.body.email));
	const password = 		rmquote (JSON.stringify (req.body.password));
	const confirm_password = 	rmquote (JSON.stringify (req.body.confirm_password));


	/* If both passwords aren't the same (typo made by user),
	 * or the user is already defined, we redirect the user to the "/register-error" route.
	 * */
	const user_exists = await prisma.user.count ({ where: { name: name }});
	if (password != confirm_password || user_exists) {
		res.redirect ("/register-error"); 
		return;
	}


	/* We create a new user, using all the data that we gathered earlier.
	 * This new record is automatically appended to the database by Prisma.
	 * */
	const user = await prisma.user.create ({
		data: {
			email:	  email,
			name: 	  name,
			password: password
		}
	});

	res.redirect ("/dashboard");
});
