app.get ('/delete-user', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}

	/* Passing username to Handlebars page */
	const username = session_user.name;
	res.render ('register/delete-user', {username});
});

app.post ('/delete-user-reply', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Remove user from any groups */
	let deleted_user_in_groups = await prisma.user_in_group.deleteMany({
		where: {
			user_id: session_user.id
		},
	});


	/* Delete the reminders created by by user */
	const deleted_reminders = await prisma.reminder.deleteMany({
		where: {
			creator_id: session_user.id
		},
	});


	/* Remove all user_in_groups linked to all groups owned by the user to delete */
	const user_groups = await prisma.group.findMany ({
		where: {
			creator_id: session_user.id
		},
	});
	for (const group of user_groups) {
		deleted_user_in_groups = await prisma.user_in_group.deleteMany({
			where: {
				group_id: group.id
			},
		});
	}


	/* Delete the groups owned by user */
	const deleted_groups = await prisma.group.deleteMany({
		where: {
			creator_id: session_user.id 
		},
	});


	/* Now delete the user (needs all previous references to creator_id to be deleted too). */
	const deleted_user = await prisma.user.deleteMany({
		where: {
			name: session_user.name
		},
	});


	/* TODO: Delete when done */
	console.log (deleted_user_in_groups);	
	console.log (deleted_reminders);
	console.log (deleted_groups);
	console.log (deleted_user);


	res.redirect ("/dashboard");
});

