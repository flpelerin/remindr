app.get ('/show-group', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}

	/* Finding all groups, owned by the session's user */
	const groups = await prisma.group.findMany ({
		where: {
			creator_id: session_user.id
		}
	});

	/* Gather all the names from all the records into a single array. */
	const group_name_array = []
	for (const elem of groups) {
		console.log (elem);					// TODO: delete when done
		
		group_name_array.push (elem.name);
	}


	/* Rendering page with specific generated information, passed to Handlebars. */
	const group_names = group_name_array.join ("; ");
	res.render ('group/show-group', {group_names});
});

app.get ('/show-group-error', async (req, res) => {
	res.render ('group/show-group-error');
});


app.post ('/show-group-reply', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Try getting the a group by name.
	 * If the group doesn't exist or the current session user isn't the group's owner,
	 * we redirect to an error page.
	 * */
	const group_name = rmquote (JSON.stringify (req.body.group_name));
	const group = await prisma.group.findFirst({
		where: {
			name: group_name
		}
	});

	if (group == null || group.creator_id != session_user.id) {
		res.redirect ("/show-group-error"); 
		return;
	}


	/* Try finding all records of user_in_group, given the group name. */
	const user_in_group = await prisma.user_in_group.findMany ({
		where: {
			group_id: group.id
		}
	});


	/* Gather usernames, linking user_in_group records to user records.
	 * Any user_id that isn't found in the user model will be ignored (no undefined name pushed).
	 * */
	const username_array = [];
	for (const elem of user_in_group) {
		console.log (elem);					// TODO: delete when done

		const user = await prisma.user.findFirst ({
			where: {
				id: elem.user_id
			}
		});

		if (user != null)
			username_array.push (user.name);
	}


	/* Rendering page with specific generated information passed to Handlebars */
	const creation_date = group.creation_date.toString ().slice (0, 15);
	const usernames = username_array.join ("; ");	// Cannot add newlines to string, passed to handlebars. TODO: Find a fix
	res.render ('group/show-group-reply', { session_user, group, creation_date, usernames });
});
