app.get ('/create-group', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined.
	 * This prevent an unlogged user from interacting with some web-pages, requiring an user to be defined.
	 * This includes all "/*-reply" routes, except "/register" and "/login"
	 * */
	const user = req.session.user;
	if (!user) {
		res.redirect ("/login"); 
		return;
	}

	res.render ('group/create-group');
});

app.get ('/create-group-error', async (req, res) => {
	res.render ('group/create-group-error');
});

app.post ('/create-group-reply', async (req, res) => {

	/* Default redirection to log-in, if no user session is defined. */
	const user = req.session.user;
	if (!user) {
		res.redirect ("/login"); 
		return;
	}

	
	/* Try getting group by group name.
	 * If the group already exists, we redirect the user (creator) to the error page.
	 * Groups are separated by group name, thus rendering them unique 
	 * */
	const group_name = rmquote (JSON.stringify (req.body.group_name));
	let group = await prisma.group.findFirst ({ where: { name: group_name } });
	if (group != null) {
		res.redirect ("/create-group-error");
		return;
	}


	/* Create the group.
	 * If we're here, that means that the group doesn't exist.
	 * */
	group = await prisma.group.create ({
		data: {
			name:		group_name,
			creator_id:	user.id,
			creation_date:	new Date ()
		}
	})


	/* Adding owner (current user) to the group, using an user_in_group record. */
	const owner_in_group = await prisma.user_in_group.create ({
		data: {
			user_id:	user.id,
			group_id:	group.id
		}
	})
	

	res.redirect ("/dashboard");
});
