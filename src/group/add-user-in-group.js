app.get ('/add-user-in-group', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const user = req.session.user;
	if (!user) {
		res.redirect ("/login"); 
		return;
	}

	res.render ('group/add-user-in-group');
});

app.get ('/add-user-in-group-error', async (req, res) => {
	res.render ('group/add-user-in-group-error');
});


app.post ('/add-user-in-group-reply', async (req, res) => {

	/* Getting group owner as current logged-in user */
	const owner = req.session.user;
	if (!owner) {
		res.redirect ("/login"); 
		return;
	}


	/* Getting group by group named, obtained from add-user-in-group POST method */
	const group_name = rmquote (JSON.stringify (req.body.group_name));
	const group = await prisma.group.findFirst({
	  where: {
	    name: group_name
	  }
	});


	/* Redirect if the current logged-in user (owner) is the actual owner of said group,
	 * or if the group doesn't actually exists.
	 * */
	if (group == null || owner.id != group.creator_id) {
		res.redirect ("/add-user-in-group-error");
		return;
	}


	/* Adds new records for each token in string
	 * If the user isn't defined or the user is already in group, ignore the record creation
	 * */
	const participants = rmquote (JSON.stringify (req.body.participants)).split (' ');
	for (const part of participants) {		
		
		/* Fetching the user from the given tokenized name.
		 * If the user doesn't exist, we skip the record creation.
		 * */
		const user = await prisma.user.findFirst ({
			where: {
				name: part 
			}
		});
		if (user == null) continue;	


		/* Fetching an user_in_group record by user id.
		 * If the record exists (user is already in group), we ignore the record creation.
		 * */
		let user_in_group = await prisma.user_in_group.findFirst ({ 
			where: { 
				user_id: user.id 
			} 
		});
		if (user_in_group != null) continue


		/* We finally add the user_in_group record, effectively adding one new user to a group. */
		user_in_group = await prisma.user_in_group.create ({
			data: {
				user_id:	user.id,
				group_id:	group.id
			}
		});
	}

	res.redirect ("/dashboard");
})

