app.get ('/delete-group', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}

	/* Finding all groups, owned by the session's user */
	const groups = await prisma.group.findMany ({
		where: {
			creator_id: session_user.id
		}
	});

	/* Gather all the names from all the records into a single array. */
	const group_name_array = []
	for (const elem of groups) {
		console.log (elem);					// TODO: delete when done
		
		group_name_array.push (elem.name);
	}


	/* Rendering page with specific generated information, passed to Handlebars. */
	const group_names = group_name_array.join ("; ");
	res.render ('group/delete-group', {group_names});
});

app.post ('/delete-group-reply', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Try getting the a group by name.
	 * If the group doesn't exist or the current session user isn't the group's owner,
	 * we redirect to an error page.
	 * */
	const group_name = rmquote (JSON.stringify (req.body.group_name));
	const group = await prisma.group.findFirst({
		where: {
			name: group_name
		}
	});

	/* Error if no group found (given its name) or the session user isn't the owner of the group */
	if (group == null || group.creator_id != session_user.id) {
		res.redirect ("/delete-group-error"); 
		return;
	}


	/* Remove all user_in_group records linked to the group to delete */
	const deleted_user_in_group = await prisma.user_in_group.deleteMany ({
		where: {
			group_id: group.id
		}
	});


	/* Delete the group, given its ID */
	const deleted_group = await prisma.group.deleteMany ({
		where: {
			id: group.id
		}
	});


	/* Redirect the user to the dashboard after deletion */
	res.redirect ('/dashboard');
});
