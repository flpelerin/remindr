app.get ('/create-reminder', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined.
	 * This prevent an unlogged user from interacting with some web-pages, requiring an user to be defined.
	 * This includes all "/*-reply" routes, except "/register" and "/login"
	 * */
	const user = req.session.user;
	if (!user) {
		res.redirect ("/login"); 
		return;
	}

	res.render ('reminder/create-reminder');
});

app.get ('/create-reminder-error', async (req, res) => {
	res.render ('reminder/create-reminder-error');
});

app.post ('/create-reminder-reply', async (req, res) => {

	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}

	
	const reminder_name = rmquote (JSON.stringify (req.body.reminder_name));	
	const reminder_due_date = req.body.moment;
	const reminder_description = rmquote (JSON.stringify (req.body.reminder_desc));
	const reminder_color = rmquote (JSON.stringify (req.body.reminder_color));
	const group_name = rmquote (JSON.stringify (req.body.group_name));


	const group = await prisma.group.findFirst ({ where: { name: group_name } });
	let reminder = await prisma.reminder.findFirst ({ where: { name: reminder_name } });


	if (reminder != null || group == null || group.creator_id != session_user.id) {
		res.redirect ("/create-reminder-error");
		return;
	}


	reminder = await prisma.reminder.create ({
		data: {
			creator_id:	session_user.id,
			group_id:	group.id,
			name:		reminder_name,
			description:	reminder_description,
			color:		reminder_color,
			due_date:	new Date (reminder_due_date),
		}
	});
	

	console.log (reminder);		// TODO: delete when done


	res.redirect ("/dashboard");
});
