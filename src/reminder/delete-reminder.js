app.get ('/delete-reminder', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Finding all reminders, owned by the session's user */
	const reminders = await prisma.reminder.findMany ({
		where: {
			creator_id: session_user.id
		}
	});


	/* Gather all the names from all the records into a single array. */
	const reminder_name_array = []
	for (const elem of reminders) {
		console.log (elem);					// TODO: delete when done
		
		reminder_name_array.push (elem.name);
	}


	/* Rendering page with specific generated information, passed to Handlebars. */
	const reminder_names = reminder_name_array.join ("; ");
	res.render ('reminder/delete-reminder', {reminder_names});
});


app.post ('/delete-reminder-reply', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Try getting the a group by name.
	 * If the group doesn't exist or the current session user isn't the reminer's owner,
	 * we redirect to an error page.
	 * */
	const reminder_name = rmquote (JSON.stringify (req.body.reminder_name));
	const reminder = await prisma.reminder.findFirst({
		where: {
			name: reminder_name
		}
	});


	console.log (reminder)						// TODO: delete when done


	/* Error if no reminder found (given its name) or the session user isn't the creator of the reminder */
	if (reminder == null || reminder.creator_id != session_user.id) {
		res.redirect ("/delete-reminder-error"); 
		return;
	}


	/* Delete the reminder, given its ID */
	const deleted_reminders = await prisma.reminder.deleteMany ({
		where: {
			id: reminder.id
		}
	});


	/* Redirect the user to the dashboard after deletion */
	res.redirect ('/dashboard');
});
