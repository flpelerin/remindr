app.get ('/show-reminder', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}


	/* Finding all reminders, owned by the session's user */
	const reminders = await prisma.reminder.findMany ({
		where: {
			creator_id: session_user.id
		}
	});


	/* Gather all the names from all the records into a single array. */
	const reminder_name_array = []
	for (const elem of reminders) {
		console.log (elem);					// TODO: delete when done
		
		reminder_name_array.push (elem.name);
	}


	/* Rendering page with specific generated information, passed to Handlebars. */
	const reminder_names = reminder_name_array.join ("; ");
	res.render ('reminder/show-reminder', {reminder_names});
});

app.get ('/show-reminder-error', async (req, res) => {
	res.render ('reminder/show-reminder-error');
});

app.post ('/show-reminder-reply', async (req, res) => {
	/* Default redirection to log-in, if no user session is defined. */
	const session_user = req.session.user;
	if (!session_user) {
		res.redirect ("/login"); 
		return;
	}

	/* Gathering reminder record by name,
	 * given that the name is defined in the previous page.
	 * Show the error page if the reminder record isn't defined or the session user isn't the creator
	 * */
	const reminder_name = rmquote (JSON.stringify (req.body.reminder_name));
	const reminder = await prisma.reminder.findFirst({
		where: {
			name: reminder_name
		}
	});

	if (reminder == null || reminder.creator_id != session_user.id) {
		res.redirect ("/show-reminder-error"); 
		return;
	}


	console.log (reminder);				// TODO: delete when done


	/* Finding the group, with a inner join to the reminder table */
	const group = await prisma.group.findFirst ({
		where: {
			id: reminder.group_id
		}
	});

	if (group == null || group.creator_id != session_user.id) {
		res.redirect ("/show-reminder-error"); 
		return;
	}


	console.log (group);				// TODO: delete when done


	/* Getting variables for posting to the next page */
	const group_name = group.name;
	const reminder_description = reminder.description;
	const reminder_due_date = reminder.due_date;
	const reminder_color = reminder.color;

	/* Loading next page */
	res.render ("reminder/show-reminder-reply", {group_name, reminder_name, reminder_description, reminder_due_date, reminder_color});
});
