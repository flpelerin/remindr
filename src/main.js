app.get ('/', async (req, res) => {
	res.redirect ("/dashboard");
});


app.get ('/debug-users', async (req, res) => {
	/* This page is exclusively reserved to admins.
	 * You should run "npx prisma generate" after each schema change.
	 * */
	const users = await prisma.user.findMany();


	/* JSON string display magic */
	res.setHeader ('Content-Type', 'text/plain')
	res.write ('Json content:\n')
	res.end (JSON.stringify (users, null, 2))
});


app.get ('/dashboard', async (req, res) => {
	res.render ('dashboard');
});




app.listen (port, () => {
	console.log (`Remindr listening on port ${port}`);
})
