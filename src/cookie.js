// Configure the session middleware
app.use(session({
  secret: 'amazing secret key',
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 60000 * 10 // Cookie expires in 10 minutes
  }
}));
