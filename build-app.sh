#!/bin/sh


SRC="src/"
TARGET="app.js"


include () { cat "$SRC/$1" >> "$TARGET"; }
clean () { rm -f "$TARGET"; }


clean

include "includes.js"
include "cookie.js"
include "main.js"

include "login/login.js"

include "register/register.js"
include "register/delete-user.js"

include "group/create-group.js"
include "group/add-user-in-group.js"
include "group/show-group.js"
include "group/delete-group.js"
include "group/delete-user-in-group.js"

include "reminder/create-reminder.js"
include "reminder/show-reminder.js"
include "reminder/delete-reminder.js"
