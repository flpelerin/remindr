DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id`			int(11)		NOT NULL,
  `name`		text		NOT NULL,
  `email`		text		NOT NULL,
  `password`		text		NOT NULL
);

ALTER TABLE `user`
  ADD			PRIMARY KEY (`id`),
  MODIFY		`id`	int(11)		NOT NULL	AUTO_INCREMENT;





DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id`			int(11)		NOT NULL,
  `creator_id`		int(11)		NOT NULL,
  `name` text				NOT NULL,
  `creation_date`	datetime	NOT NULL	DEFAULT	current_timestamp()
);

ALTER TABLE `group`
  ADD 			PRIMARY KEY (`id`),
  MODIFY		`id`	int(11)		NOT NULL	AUTO_INCREMENT;





DROP TABLE IF EXISTS `user_in_group`;
CREATE TABLE `user_in_group` (
  `user_id`		int(11)		NOT NULL,
  `group_id`		int(11)		NOT NULL
);

ALTER TABLE `user_in_group`
  ADD			PRIMARY KEY	(`user_id`,`group_id`);





DROP TABLE IF EXISTS `reminder`;
CREATE TABLE `reminder` (
	`id`		int(11) 	NOT NULL,
	`creator_id`	int(11) 	NOT NULL,
	`group_id`	int(11) 	NOT NULL,
	`name` text			NOT NULL,
	`description`	text		NOT NULL,
	`color`		varchar(7)	NOT NULL,
	`due_date`	datetime	NOT NULL	DEFAULT	current_timestamp()
);

ALTER TABLE `reminder`
  ADD 			PRIMARY KEY (`id`),
  MODIFY		`id`	int(11)		NOT NULL	AUTO_INCREMENT;
